from sentrycommon import SentryAWSClients
import json


def lambda_handler(event, _):
    imageid = None
    num = None
    if event["para"].endswith('.jpg'):
        imageid = event["para"]
    else:
        num = int(event["para"])
    destination_bucket_name = 'sentry-golden-dataset'
    r_list = []
    url_dict = {}
    t_list = []
    j_list = []

    sentry_s3 = SentryAWSClients.SentryS3()
    return_output = {}

    if num is not None:
        resp = sentry_s3.s3_client.list_objects(Bucket=destination_bucket_name, Prefix='labels/')
        for keys in resp['Contents']:
            key = keys['Key']
            if key.endswith('.jpg'):
                r_list.append(key)
            if key.endswith('.json'):
                j_list.append(key.strip('.json'))
        for x in j_list:
            r_list.remove(x)
        print(r_list)
        if num > len(r_list):
            num = len(r_list)
        for i in range(num):
            t_list.append(r_list[i])
        for key in t_list:
            # img_bytes = sentry_s3.get_from_s3(destination_bucket_name, key)
            img_key = key.strip('/labels')
            print(img_key)
            # sentry_s3.put_into_s3(destination_bucket_name, "labels/" + img_key, img_bytes)
            # resp = sentry_s3.s3_client.delete_object(Bucket=destination_bucket_name, Key=key)
            url = sentry_s3.get_timed_s3_url(destination_bucket_name, "labels/" + img_key, 3600)
            url_dict.update({img_key: url})
            return_output = url_dict
            print(return_output)

    if imageid is not None:
        # keyjson = None
        # for keys in resp['Contents']:
        #     key = keys['Key']
        #     key = key.strip('labels/')
        #     if key.startswith(imageid) and key.endswith('.json'):
        #         print(key)
        #         keyjson = key
        # if keyjson is not None:

        key_json = imageid + '.json'
        data = json.loads(sentry_s3.get_from_s3(destination_bucket_name, "labels/" + key_json))
        num_people = data["num_people"]
        url = sentry_s3.get_timed_s3_url(destination_bucket_name, "labels/" + imageid, 3600)
        return_output = {"key": imageid,
                         "url": url,
                         "num_people": num_people}
        # else:
        #     return_output = "Error! Key not found"
        print(return_output)

    return return_output
