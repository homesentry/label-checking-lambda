from __future__ import print_function
import sys
import boto3
from botocore.exceptions import ClientError
import os
from sentrycommon import PipelineConfig


def publish_new_version(artifact):
    """
    Publishes new version of the AWS Lambda Function
    """
    try:
        client = boto3.client('lambda', region_name="us-west-2", aws_access_key_id=PipelineConfig.AWS_ACCESS_KEY,
                       aws_secret_access_key=PipelineConfig.AWS_SECRET_KEY)
    except ClientError as err:
        print("Failed to create boto3 client.\n" + str(err))
        return False

    try:
        response = client.update_function_code(
            FunctionName=os.environ['AWS_LAMBDA_FUNCTION_NAME'],
            ZipFile=open(artifact, 'rb').read(),
            Publish=True
        )
        return response
    except ClientError as err:
        print("Failed to update function code.\n" + str(err))
        return False
    except IOError as err:
        print("Failed to access " + artifact + ".\n" + str(err))
        return False

def main():
    " Your favorite wrapper's favorite wrapper "
    if not publish_new_version('/tmp/artifact.zip'):
        sys.exit(1)

if __name__ == "__main__":
    main()
