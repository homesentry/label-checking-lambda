import os

os.environ['pipeline'] = 'Production'
import lambda_function
import pytest

# TODO fails because code isn't working
# def test_create_image_link_from_number():
#     event = {"para": "10"}
#     return_output = lambda_function.lambda_handler(event, None)
#     assert event["para"] == "10"
#     assert len(return_output) == 10


def test_from_imageid():
    event = {"para": "5VEGNW-B_PoolGate_1572879547.jpg"}
    return_output = lambda_function.lambda_handler(event, None)
    print(return_output)
    assert event["para"] == "5VEGNW-B_PoolGate_1572879547.jpg"
    assert return_output["key"] is "5VEGNW-B_PoolGate_1572879547.jpg"


def test_wrong_imageid():
    event = {"para": "5VEGNW-B_PoolGate_157.jpg"}

    with pytest.raises(Exception) as e_info:
        return_output = lambda_function.lambda_handler(event, None)

    assert event["para"] == "5VEGNW-B_PoolGate_157.jpg"


def test_from_another_imageid():
    event = {"para": "R27VA5-B_SC_1573392401.jpg"}
    return_output = lambda_function.lambda_handler(event, None)
    print(return_output)
    assert event["para"] == "R27VA5-B_SC_1573392401.jpg"
    assert return_output["key"] is "R27VA5-B_SC_1573392401.jpg"
